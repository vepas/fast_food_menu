import 'dart:math';

import 'package:fast_food_menu/constants/colors.dart';

import '/components/image_background.dart';
import '/components/widgets/confirm_button.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
// import 'package:flutter/services.dart';

class ImageDownloaderPage extends StatefulWidget {
  ImageDownloaderPage({Key key}) : super(key: key);

  @override
  _ImageDownloaderPageState createState() => _ImageDownloaderPageState();
}

class _ImageDownloaderPageState extends State<ImageDownloaderPage> {
  double ratio;
  int progressValue = 0;
  bool started = false;

  @override
  void initState() {
    super.initState();
    MenuProvider model = Provider.of<MenuProvider>(context, listen: false);
    if (model.storedImageCount == model.imageCount)
      model.emptyStoredImageCount();
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;

    return Consumer<MenuProvider>(builder: (context, model, child) {
      if (model.imageCount > 0) {
        progressValue = int.parse(
            (model.storedImageCount / model.imageCount * 100)
                .toStringAsFixed(0));
        if (progressValue > 0 && progressValue < 100) started = true;
      }
      return Scaffold(
        body: ImageBackground(
          child: Container(
            width: 468 * ratio,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xFF1F2328),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  height: 52 * ratio,
                  padding: EdgeInsets.only(left: 26, right: 5),
                  decoration: BoxDecoration(
                    color: Color(0xFF2C3137),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        model.getTranslation('Download images'),
                        style: TextStyle(
                          fontSize: 16 * ratio,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                        icon: Transform.rotate(
                          angle: pi / 4,
                          child: Icon(Icons.add,
                              color: Color(0xFFABBBC2), size: 24 * ratio),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 30),
                  child: Column(
                    children: [
                      Text(
                        started
                            ? progressValue < 100
                                ? model.getTranslation(
                                    'Making last changes, please wait a minute')
                                : model.getTranslation(
                                    'Download has been completed')
                            : model.getTranslation(
                                'Click the download button for latest update'),
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child:
                            Text(started ? progressValue.toString() + '%' : '',
                                style: TextStyle(
                                  fontSize: 16 * ratio,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                )),
                      ),
                      Container(
                        width: 383 * ratio,
                        height: 8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xFF393C49),
                        ),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            width: 383 * 0.01 * progressValue * ratio,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: UIColors().primary
                                // gradient: LinearGradient(colors: [
                                //   Color(0xFFF89B32),
                                //   Color(0xFFFC542D),
                                // ]),
                                ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      progressValue == 100
                          ? ConfirmButton(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              text: model.getTranslation('Continue'),
                            )
                          : started
                              ? Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 128,
                                    vertical: 14,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Color(0xFFABBBC2),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Text(
                                    model.getTranslation('Wait...'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              : ConfirmButton(
                                  onTap: () async {
                                    imageCache.clear();
                                    setState(() {
                                      started = true;
                                      model.getFoods(ratio);
                                      model.getDetails();
                                      model.getTables();
                                    });
                                  },
                                  text: model.getTranslation('Download'),
                                )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
