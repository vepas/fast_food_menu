import 'dart:math';

import 'package:flutter/services.dart';

import '/components/image_background.dart';
import '/components/widgets/confirm_button.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IpChangerPage extends StatefulWidget {
  IpChangerPage({Key key}) : super(key: key);

  @override
  IpChangerPageState createState() => IpChangerPageState();
}

class IpChangerPageState extends State<IpChangerPage> {
  double ratio;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    ratio = MediaQuery.of(context).size.width / 1112;

    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Scaffold(
        body: ImageBackground(
          child: Container(
            width: 468 * ratio,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xFF1F2328),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  height: 52 * ratio,
                  padding: EdgeInsets.only(left: 26, right: 5),
                  decoration: BoxDecoration(
                    color: Color(0xFF2C3137),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        model.getTranslation('Change IP'),
                        style: TextStyle(
                          fontSize: 16 * ratio,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                        icon: Transform.rotate(
                          angle: pi / 4,
                          child: Icon(
                            Icons.add,
                            color: Color(0xFFABBBC2),
                            size: 24 * ratio,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 30),
                  child: Column(
                    children: [
                      SizedBox(
                        width: 340,
                        child: Form(
                          key: _formKey,
                          child: TextFormField(
                            validator: (value) {
                              if (value.length > 0 && value.length < 5)
                                return 'IP must be longer';
                              return null;
                            },
                            onSaved: (value) {
                              if (value.length > 0) model.changeIP(value);
                            },
                            cursorColor: Colors.white,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintText: model.ip,
                              hintStyle: TextStyle(
                                color: Color(0xFFABBBC2),
                              ),
                              filled: true,
                              fillColor: Color(0xFF2C3137),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFF393C49), width: 1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFF393C49), width: 1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor,
                                    width: 1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      ConfirmButton(
                        onTap: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            Navigator.pop(context);
                          }
                        },
                        text: model.getTranslation('Confirm'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
