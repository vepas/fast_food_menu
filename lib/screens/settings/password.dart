import 'dart:math';

import 'package:fast_food_menu/constants/colors.dart';
import 'package:flutter/services.dart';

import '/components/image_background.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class PasswordPage extends StatefulWidget {
  final bool fromTable;
  PasswordPage({this.fromTable = false});

  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  double ratio;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Scaffold(
        body: ImageBackground(
          child: Container(
            width: 468 * ratio,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xFF1F2328),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  height: 52 * ratio,
                  padding: EdgeInsets.only(left: 26, right: 5),
                  decoration: BoxDecoration(
                    color: Color(0xFF2C3137),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        model.getTranslation('Enter password'),
                        style: TextStyle(
                          fontSize: 16 * ratio,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                        icon: Transform.rotate(
                          angle: pi / 4,
                          child: Icon(
                            Icons.add,
                            color: Color(0xFFABBBC2),
                            size: 24 * ratio,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 20),
                    Text(
                      model.getTranslation(
                        'Enter password to get in to settings',
                      ),
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 20 * ratio,
                        bottom: 30 * ratio,
                      ),
                      width: 340,
                      child: Form(
                        key: _formKey,
                        child: PinCodeTextField(
                          appContext: context,
                          length: 5,
                          onChanged: (value) {},
                          autovalidateMode: AutovalidateMode.disabled,
                          validator: (value) {
                            if (value == model.password) return null;
                            return 'wrong password';
                          },
                          enableActiveFill: true,
                          showCursor: false,
                          keyboardType: TextInputType.number,
                          backgroundColor: Colors.transparent,
                          textStyle: TextStyle(
                            color: Colors.white,
                          ),
                          pinTheme: PinTheme(
                            fieldWidth: 48,
                            fieldHeight: 48,
                            borderWidth: 1,
                            inactiveColor: Color(0xFF393C49),
                            activeColor: Color(0xFF393C49),
                            selectedColor: UIColors().primary,
                            activeFillColor: Color(0xFF2C3137),
                            inactiveFillColor: Color(0xFF2C3137),
                            selectedFillColor: Color(0xFF2C3137),
                            borderRadius: BorderRadius.circular(8),
                            shape: PinCodeFieldShape.box,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) if (widget
                            .fromTable) {
                          model.getTables();
                          if (model.halls[0] != null &&
                              model.selectedHall == null)
                            model.selectHall(model.halls[0]);
                          Navigator.pushReplacementNamed(
                            context,
                            'tables',
                          );
                        } else {
                          Navigator.pushReplacementNamed(
                            context,
                            'settings',
                          );
                        }
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 132, vertical: 14),
                        decoration: BoxDecoration(
                          // gradient: LinearGradient(
                          //   colors: [
                          //     Color(0xFFF89B32),
                          //     Color(0xFFFC542D),
                          //   ],
                          // ),
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xfffed931),
                          /* boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 24,
                                color: Color(0xFFEA7C69).withOpacity(0.3),
                              )
                            ] */
                        ),
                        child: Text(
                          model.getTranslation('Confirm'),
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 32),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
