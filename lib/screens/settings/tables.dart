import '/constants/colors.dart';
import '/constants/text.dart';
import '/components/widgets/table_card.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TablesPage extends StatefulWidget {
  TablesPage({Key key}) : super(key: key);

  @override
  _TablesPageState createState() => _TablesPageState();
}

class _TablesPageState extends State<TablesPage>
    with SingleTickerProviderStateMixin {
  double ratio;
  UIText uiText;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      initialIndex:
          Provider.of<MenuProvider>(context, listen: false).selectedHallIndex,
      length: Provider.of<MenuProvider>(context, listen: false).halls.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        return Scaffold(
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            padding: EdgeInsets.only(right: 19 * ratio),
                            icon: Icon(Icons.arrow_back, size: 19),
                          ),
                          Text(
                            model.getTranslation('Back'),
                            style: TextStyle(
                              fontSize: 28 * ratio,
                              fontWeight: FontWeight.w500,
                              height: 1,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            model.getTranslation('Choosen table :') + ' ',
                            style: TextStyle(
                              fontSize: 20 * ratio,
                            ),
                          ),
                          UIText(
                            context: context,
                            text: model.selectedTable.number < 10
                                ? '0' + model.selectedTable.number.toString()
                                : model.selectedTable.number.toString(),
                            color: UIColors().primary,
                          ).h2,
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 25),
                  TabBar(
                    onTap: (index) {
                      model.selectHall(model.halls[index]);
                    },
                    controller: _tabController,
                    isScrollable: true,
                    // indicatorColor: UIColors().primary,
                    // labelPadding: EdgeInsets.only(right: 60, bottom: 5),
                    // indicatorSize: TabBarIndicatorSize.tab,
                    indicator: BoxDecoration(
                        border: Border.all(width: 1, color: UIColors().primary),
                        borderRadius: BorderRadius.circular(8),
                        color: Color(0xff1E1E20)),
                    // indicatorPadding: EdgeInsets.only(right: 80),
                    labelStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16 * ratio,
                      color: Colors.white,
                    ),
                    unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16 * ratio,
                      color: UIColors().passive,
                    ),
                    tabs: List.generate(
                      model.halls.length,
                      (index) => ConstrainedBox(
                        constraints: BoxConstraints(minWidth: 30),
                        child: SizedBox(
                            height: 45 * ratio,
                            child: Center(
                              child: Text(
                                  model.halls[index].name[model.selectedLang]),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    child: GridView.count(
                      crossAxisCount: 5,
                      childAspectRatio: 194 / 120,
                      mainAxisSpacing: 20 * ratio,
                      crossAxisSpacing: 20 * ratio,
                      padding: EdgeInsets.only(top: 20),
                      children: List.generate(
                        model.selectedHall.tables.length,
                        (index) =>
                            TableCard(table: model.selectedHall.tables[index]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
