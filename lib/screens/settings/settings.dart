import 'dart:math';

import '/components/image_background.dart';
import '/services/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  double ratio;

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Scaffold(
        backgroundColor: Colors.grey,
        body: ImageBackground(
          child: Container(
            width: 251 * ratio,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Color(0xFF1F2328),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  height: 52 * ratio,
                  padding: EdgeInsets.only(left: 26, right: 5),
                  decoration: BoxDecoration(
                    color: Color(0xFF2C3137),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        model.getTranslation('Settings'),
                        style: TextStyle(
                          fontSize: 16 * ratio,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                        icon: Transform.rotate(
                          angle: pi / 4,
                          child: Icon(
                            Icons.add,
                            color: Color(0xFFABBBC2),
                            size: 24 * ratio,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 35),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, 'ip_changer');
                        },
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(vertical: 20),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xFF393C49),
                              ),
                            ),
                          ),
                          child: Text(
                            model.getTranslation('Change IP'),
                            style: TextStyle(
                              fontSize: 16 * ratio,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, 'image_downloader');
                        },
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(vertical: 20),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xFF393C49),
                              ),
                            ),
                          ),
                          child: Text(
                            model.getTranslation('Download'),
                            style: TextStyle(
                              fontSize: 16 * ratio,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, 'tables');
                        },
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(vertical: 20),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Color(0xFF393C49),
                              ),
                            ),
                          ),
                          child: Text(
                            model.getTranslation('Change table'),
                            style: TextStyle(
                              fontSize: 16 * ratio,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              model.getTranslation('Image'),
                              style: TextStyle(
                                fontSize: 16 * ratio,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                              ),
                            ),
                            CupertinoSwitch(
                                activeColor: Color(0xFF3e3e3e),
                                value: model.withImage,
                                onChanged: (value) {
                                  model.toggleWithImageState(value);
                                }),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              model.getTranslation('Make order'),
                              style: TextStyle(
                                fontSize: 16 * ratio,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                              ),
                            ),
                            CupertinoSwitch(
                                activeColor: Color(0xFF3e3e3e),
                                // trackColor: Color(0xFFFC542D),
                                value: model.makeOrderState,
                                onChanged: (value) {
                                  model.toggleMakeOrderState(value);
                                }),
                          ],
                        ),
                      ),
                      Text(
                        'v 1.0.1',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12 * ratio,
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
