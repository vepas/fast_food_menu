import '/constants/colors.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double ratio;

  List<String> languages = ['tm', 'ru', 'en'];

  getLanguageString(String countryCode) {
    switch (countryCode) {
      case 'tm':
        return 'Türkmen';
        break;
      case 'ru':
        return 'Русский';
        break;
      case 'en':
        return 'English';
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Scaffold(
        backgroundColor: Colors.grey,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            padding: EdgeInsets.all(11),
            decoration: BoxDecoration(
              color: Color(0xFF1A1D22).withOpacity(0.7),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: 100),
                // Container(
                //   height: 150,
                //   width: 200,
                //   child: Image.asset(
                //     'assets/images/icon.png',
                //     fit: BoxFit.cover,
                //   ),
                // ),
                Expanded(
                  child: Center(
                    child: Container(
                      // height: 226 * ratio,
                      width: 251 * ratio,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Color(0xFF1F2328),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 52 * ratio,
                            decoration: BoxDecoration(
                              color: Color(0xFF2C3137),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                topRight: Radius.circular(8),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                'Dil Saýlaň',
                                style: TextStyle(
                                  fontSize: 16 * ratio,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                          Center(
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 10),
                              child: Column(
                                children: List.generate(
                                  languages.length,
                                  (index) {
                                    return InkWell(
                                      onTap: () {
                                        model.selectLang(languages[index]);
                                        model.getOrders();
                                        Navigator.pushNamed(context, 'menu');
                                      },
                                      child: Container(
                                        width: 182 * ratio,
                                        padding: EdgeInsets.only(
                                            top: 15, bottom: 11),
                                        decoration: BoxDecoration(
                                          border: Border.symmetric(
                                            horizontal: BorderSide(
                                              color: index == 1
                                                  ? Color(0xFF393C49)
                                                  : Colors.transparent,
                                              width: 1,
                                            ),
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            getLanguageString(
                                              languages[index],
                                            ),
                                            style: TextStyle(
                                              fontSize: 16 * ratio,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 50),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    IconButton(
                      icon: SvgPicture.asset('assets/icons/settings.svg'),
                      onPressed: () {
                        Navigator.of(context).pushNamed('password');
                      },
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          'Powered by Pikir',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20 * ratio,
                          ),
                        ),
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('passwordFromTable');
                      },
                      icon: Icon(
                        Icons.grid_view_outlined,
                        color: UIColors().passive,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
