import 'package:fast_food_menu/models/category.dart';

import '/components/widgets/category_card_without_img.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/components/widgets/category_card.dart';
// import '/screens/settings/password.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class CategoriesPage extends StatefulWidget {
  CategoriesPage({Key key}) : super(key: key);

  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Material(
        color: UIColors().background,
        child: Padding(
          padding: EdgeInsets.only(top: 30, left: 33, right: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          model.mainNavKey.currentState.pop();
                        },
                        icon: Icon(Icons.arrow_back),
                        iconSize: 19,
                        highlightColor: UIColors().cartBackground,
                        padding: EdgeInsets.only(
                            left: 0, right: 0, top: 0, bottom: 0),
                      ),
                      UIText(
                        context: context,
                        text: model.getTranslation('Category'),
                      ).h1,
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      model.mainNavKey.currentState
                          .pushNamed('passwordFromTable');
                    },
                    child: Container(
                      height: 58,
                      width: 58,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(
                          width: 1,
                          color: Color(0xff393C49),
                        ),
                      ),
                      child: Center(
                        child:
                            SvgPicture.asset('assets/icons/table_button.svg'),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Expanded(
                child: GridView.count(
                  crossAxisCount: 3,
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  childAspectRatio: model.withImage ? 215 / 175 : 318 / 119,
                  mainAxisSpacing: 15,
                  crossAxisSpacing: 15,
                  children: List.generate(
                    model.categories.length,
                    (index) => model.withImage
                        ? CategoryCard(
                            category: model.categories[index],
                            // isSubcategory: false,
                          )
                        : CategoryCardWithoutImage(
                            category: model.categories[index],
                          ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              )
            ],
          ),
        ),
      );
    });
  }
}
