import 'dart:math';

import '/components/widgets/basket_details_popup.dart';
import '/components/widgets/basket_item_card.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'orders.dart';

class BasketPage extends StatefulWidget {
  BasketPage({Key key}) : super(key: key);

  @override
  _BasketPageState createState() => _BasketPageState();
}

class _BasketPageState extends State<BasketPage>
    with SingleTickerProviderStateMixin {
  double ratio;
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );
    animation = Tween<double>(begin: 0, end: 1).animate(controller);
    // controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Row(
        children: [
          GestureDetector(
            onTap: () {
              // controller.reverse();
              Navigator.pop(context);
            },
            child: Container(
              width: 54 * ratio,
              color: UIColors().basketBackground,
              child: Center(
                child: AnimatedBuilder(
                  animation: animation,
                  builder: (context, child) {
                    return Transform.rotate(
                      // angle: pi * animation.value,
                      angle: pi,
                      child: child,
                    );
                  },
                  child: SvgPicture.asset('assets/icons/double_arrow.svg'),
                ),
              ),
            ),
          ),
          Container(
            width: 1,
            color: UIColors().stroke,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30),
              color: UIColors().basketBackground,
              child: Navigator(
                initialRoute: 'basket',
                onGenerateRoute: (settings) {
                  Widget page = BasketPage2();
                  if (settings.name == 'orders') page = OrdersPage();
                  return PageRouteBuilder(
                      transitionDuration: Duration(milliseconds: 100),
                      reverseTransitionDuration: Duration(milliseconds: 100),
                      pageBuilder: (context, animation, animation1) {
                        return FadeTransition(opacity: animation, child: page);
                      });
                },
              ),
            ),
          ),
        ],
      );
    });
  }
}

class BasketPage2 extends StatefulWidget {
  BasketPage2({Key key}) : super(key: key);

  @override
  _BasketPage2State createState() => _BasketPage2State();
}

class _BasketPage2State extends State<BasketPage2> {
  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    MenuProvider model = Provider.of<MenuProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(
              height: 48 * ratio,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIText(context: context, text: model.getTranslation('Basket'))
                      .h2,
                  UIText(
                    context: context,
                    text: model.hal == null || model.hal == {}
                        ? '#${model.selectedTable.number} ${model.getTranslation('Table')} - ${model.selectedHall.name[model.selectedLang]}'
                        : '#${model.selectedTable.number} ${model.getTranslation('Table')} - ${model.hal[model.selectedLang]}',
                    color: UIColors().passive,
                  ).bodyText2,
                ],
              ),
            ),
            Expanded(
              child: SizedBox(),
            ),
            AnimatedSwitcher(
              duration: Duration(milliseconds: 100),
              child: model.basket.length > 0
                  ? GestureDetector(
                      onTap: () {
                        model.emptyBasket();
                      },
                      child: Container(
                        height: 48 * ratio,
                        width: 125 * ratio,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: UIColors().cartColor
                            // border: Border.all(color: UIColors().primary),
                            ),
                        child: Center(
                          child: UIText(
                                  context: context,
                                  text: model.getTranslation('Clear'),
                                  color: UIColors().primary)
                              .bodyText2,
                        ),
                      ),
                    )
                  : SizedBox(),
            ),
            model.makeOrderState
                ? GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, 'orders');
                    },
                    child: Container(
                      height: 48 * ratio,
                      width: 48 * ratio,
                      margin: EdgeInsets.only(left: 15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: UIColors().cartColor
                          // border: Border.all(color: UIColors().primary),
                          ),
                      child: Center(
                        child: SvgPicture.asset(
                          'assets/icons/history.svg',
                          color: UIColors().primary,
                          height: 20 * ratio,
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
        Container(
            height: 1,
            width: double.infinity,
            margin: EdgeInsets.only(top: 18),
            color: UIColors().stroke),
        Expanded(
          child: model.basket.length > 0
              ? RawScrollbar(
                  thickness: 4 * ratio,
                  isAlwaysShown: true,
                  radius: Radius.circular(8),
                  thumbColor: UIColors().primary,
                  child: AnimatedList(
                      padding: EdgeInsets.only(top: 20),
                      key: model.listKey,
                      initialItemCount: model.basket.length,
                      itemBuilder: (context, index, animation) => Container(
                            height: 70 * ratio,
                            margin: EdgeInsets.only(right: 10 * ratio),
                            child: BasketItemCard(
                              basketItem: model.basket[index],
                              animation: animation,
                            ),
                          )),
                )
              // ListView.builder(
              //     itemCount: model.basket.length,
              //     itemBuilder: (context, index) {
              //       return BasketItemCard(basketItem: model.basket[index]);
              //     },
              //   )
              : Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SvgPicture.asset(
                        'assets/icons/No food Icon.svg',
                        height: 64 * ratio,
                      ),
                      SizedBox(height: 15),
                      UIText(
                              context: context,
                              text: model.getTranslation('No choosen meals'))
                          .bodyTextPassive2,
                    ],
                  ),
                ),
        ),
        model.basket.length == 0
            ? Container()
            : Container(
                // height: 240 * ratio,
                child: Column(
                  children: [
                    Container(
                        height: 1,
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 7),
                        color: UIColors().stroke),
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        UIText(
                                context: context,
                                text: model.getTranslation('Cost'),
                                color: UIColors().passive)
                            .bodyText2,
                        UIText(
                                context: context,
                                text:
                                    '${model.generatePrice(model.totalCost)} TMT')
                            .bodyText2,
                      ],
                    ),
                    SizedBox(height: 8),
                    model.details['service']['cost'] > 0
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              UIText(
                                      context: context,
                                      text: model.getTranslation('Service') +
                                          ' ' +
                                          (model.details['service']['unit'] ==
                                                  'manat'
                                              ? ''
                                              : '(${model.details['service']['cost']}%)'),
                                      color: UIColors().passive)
                                  .bodyText2,
                              UIText(
                                      context: context,
                                      text: model.details['service']['unit'] ==
                                              'manat'
                                          ? '+${model.generatePrice(model.details['service']['cost'])} TMT'
                                          : '+${model.generatePrice((model.totalCost * model.details['service']['cost'] / 100).round())} TMT')
                                  .bodyText2,
                            ],
                          )
                        : SizedBox(),
                    SizedBox(
                        height: model.details['service']['cost'] > 0 ? 8 : 0),
                    Container(
                        height: 1,
                        width: 218 * ratio,
                        margin: EdgeInsets.symmetric(vertical: 10),
                        color: UIColors().stroke),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        UIText(
                                context: context,
                                text: model.getTranslation('Total Cost'),
                                color: UIColors().passive)
                            .foodNameDemo,
                        UIText(
                                context: context,
                                text:
                                    '${model.generatePrice(model.totalCost + (model.details['service']['unit'] == 'manat' ? model.details['service']['cost'] : (model.totalCost * model.details['service']['cost'] / 100).round()))} TMT')
                            .foodNameDemo,
                      ],
                    ),
                    SizedBox(height: 20),
                    InkWell(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return BasketDetailsPopup();
                            });
                      },
                      child: Container(
                        width: 321,
                        padding:
                            EdgeInsets.symmetric(horizontal: 0, vertical: 14),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: UIColors().primary),
                        child: Center(
                          child: Text(
                            model.makeOrderState
                                ? model.getTranslation('Make order')
                                : model.getTranslation('All details'),
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ],
    );
  }
}
