import '/components/widgets/order_expansion_panel.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class OrdersPage extends StatefulWidget {
  OrdersPage({Key key}) : super(key: key);

  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  double ratio;

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      print(model.orders.length);
      return Material(
        color: UIColors().basketBackground,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    InkWell(
                        child: Icon(
                          Icons.arrow_back,
                          size: 19,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    SizedBox(
                      height: 48 * ratio,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                                  context: context,
                                  text: model.getTranslation('History'))
                              .h2,
                          UIText(
                            context: context,
                            text:
                                '#${model.selectedTable.number} ${model.getTranslation('Table')} - ${model.selectedHall.name[model.selectedLang]}',
                            color: UIColors().passive,
                          ).bodyText2,
                        ],
                      ),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 48 * ratio,
                    width: 48 * ratio,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: UIColors().primary),
                    ),
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/icons/basket.svg',
                        color: UIColors().primary,
                        height: 20 * ratio,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
                height: 1,
                width: double.infinity,
                margin: EdgeInsets.only(top: 18),
                color: UIColors().stroke),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(0),
                children: List.generate(
                  model.orders.length,
                  (index) => OrderExpansionPanel(index),
                ),
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              height: 60 * ratio,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  UIText(
                          context: context,
                          text: model
                              .getTranslation('Total Cost (all together) :'),
                          color: UIColors().passive)
                      .foodNameDemo,
                  UIText(
                          context: context,
                          text: model.ordersTotalCost > 0
                              ? model.generatePrice(model.ordersTotalCost) +
                                  ' TMT'
                              : '0' + ' TMT')
                      .foodNameDemo,
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
