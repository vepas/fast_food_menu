import 'package:flutter_svg/flutter_svg.dart';

import '/components/widgets/food_card.dart';
import '/components/widgets/food_card_without_img.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubcategoryPage extends StatefulWidget {
  final bool isDrink;

  SubcategoryPage({Key key, this.isDrink = false}) : super(key: key);

  @override
  _SubcategoryPageState createState() => _SubcategoryPageState();
}

class _SubcategoryPageState extends State<SubcategoryPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    super.initState();
    MenuProvider model = Provider.of<MenuProvider>(context, listen: false);
    tabController = TabController(
        vsync: this,
        length: model.categories.length,
        initialIndex: model.selectedCategoryIndex);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.isDrink);
    // ignore: unused_local_variable
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        return Material(
          color: UIColors().background,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Container(
              //   height: 1,
              //   color: UIColors().stroke,
              // ),
              Padding(
                padding: EdgeInsets.only(left: 15, right: 30),
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                                model.selectSubcategory = null;
                                print('boo');
                              },
                              icon: Icon(Icons.arrow_back),
                              iconSize: 19,
                              padding: EdgeInsets.only(
                                  left: 0, right: 14, top: 0, bottom: 0),
                            ),
                            UIText(
                              context: context,
                              text: model.selectedSubcategory == null
                                  ? ''
                                  : model.selectedSubcategory
                                      .name[model.selectedLang],
                            ).h2,
                          ],
                        ),
                        GestureDetector(
                          onTap: () {
                            model.mainNavKey.currentState
                                .pushNamed('passwordFromTable');
                          },
                          child: Container(
                            height: 58,
                            width: 58,
                            margin: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                              // color: Colors.transparent,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                width: 2,
                                color: Color(0xff393C49),
                              ),
                            ),
                            child: Center(
                              child: SvgPicture.asset(
                                'assets/icons/table_button.svg',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: GridView.count(
                  padding: EdgeInsets.only(bottom: 15),
                  crossAxisCount:
                      // model.withImage
                      //     ? 2
                      //     :
                      widget.isDrink ? 3 : 3,
                  // padding: EdgeInsets.only(top: 30, left: 33, right: 30),
                  childAspectRatio: !model.withImage
                      ? 318 / 127
                      : /* widget.isDrink
                          ? 3072 / 4602
                          : */
                      255 / 184,
                  mainAxisSpacing: 30,
                  crossAxisSpacing: 30,
                  children: List.generate(
                    model.selectedSubcategory == null
                        ? 0
                        : model.selectedSubcategory.foods.length,
                    (index) => model.withImage
                        ? FoodCard(
                            food: model.selectedSubcategory.foods[index],
                            isDrink: widget.isDrink)
                        : FoodCardWithoutImage(
                            food: model.selectedSubcategory.foods[index]),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
