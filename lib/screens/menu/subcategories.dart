import '/components/widgets/category_card_without_img.dart';
import '/components/widgets/food_card.dart';
import '/components/widgets/food_card_without_img.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/components/widgets/category_card.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    super.initState();
    MenuProvider model = Provider.of<MenuProvider>(context, listen: false);
    tabController = TabController(
        vsync: this,
        length: model.categories.length,
        initialIndex: model.selectedCategoryIndex);
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    double ratio = MediaQuery.of(context).size.width / 1112;

    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        tabController.animateTo(model.selectedCategoryIndex);
        return Material(
          color: UIColors().background,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Container(
              //   height: 1,
              //   color: UIColors().stroke,
              // ),
              Padding(
                padding: EdgeInsets.only(left: 15, right: 30),
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                model.selectCategory = null;
                                Navigator.pop(context);
                                print('booom');
                              },
                              icon: Icon(Icons.arrow_back),
                              iconSize: 19,
                              padding: EdgeInsets.only(
                                  left: 0, right: 14, top: 0, bottom: 0),
                            ),
                            UIText(
                              context: context,
                              text: model.selectedCategory != null
                                  ? model
                                      .selectedCategory.name[model.selectedLang]
                                  : '',
                            ).h2,
                          ],
                        ),
                        GestureDetector(
                          onTap: () {
                            model.mainNavKey.currentState
                                .pushNamed('passwordFromTable');
                          },
                          child: Container(
                            height: 58, 
                            width: 58,
                            margin: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                              // color: Colors.transparent,
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                width: 2,
                                color: Color(0xff393C49),
                              ),
                            ),
                            child: Center(
                              child: SvgPicture.asset(
                                'assets/icons/table_button.svg',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: (model.selectedCategory == null &&
                        model.selectedSubcategory == null)
                    ? Container()
                    : model.selectedCategory.subcategories.length > 0
                        ? GridView.count(
                            cacheExtent: 99,
                            crossAxisCount: 3,
                            padding: EdgeInsets.only(
                                bottom: 15, top: 30, /* left: 33 */ right: 30),
                            childAspectRatio:
                                !model.withImage ? 318 / 119 : 467 / 350,
                            mainAxisSpacing: 30,
                            crossAxisSpacing: 30,
                            children: List.generate(
                              model.selectedCategory.subcategories.length,
                              (index) => model.withImage
                                  ? CategoryCard(
                                      category: model.selectedCategory
                                          .subcategories[index],
                                      // isSubcategory: true,
                                    )
                                  : CategoryCardWithoutImage(
                                      category: model.selectedCategory
                                          .subcategories[index],
                                    ),
                            ),
                          )
                        : GridView.count(
                            cacheExtent: 99,
                            crossAxisCount: model.withImage
                                ? model.selectedCategory.isDrink
                                    ? 3
                                    : 3
                                : 3,
                            padding: EdgeInsets.only(right: 20, bottom: 15),
                            childAspectRatio: !model.withImage
                                ? 318 / 127
                                /* model.selectedCategory.isDrink
                                ? 3072 / 4602 */
                                : 255 / 184,
                            mainAxisSpacing: 20,
                            crossAxisSpacing: 20,
                            children: List.generate(
                              model.selectedCategory.foods.length,
                              (index) => model.withImage
                                  ? FoodCard(
                                      food: model.selectedCategory.foods[index],
                                      isDrink: model.selectedCategory.isDrink)
                                  : FoodCardWithoutImage(
                                      food:
                                          model.selectedCategory.foods[index]),
                            ),
                          ),
              ),
            ],
          ),
        );
      },
    );
  }
}
