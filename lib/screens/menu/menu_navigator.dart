import 'package:fast_food_menu/components/widgets/side_category_card.dart';

import '/components/widgets/side_basket.dart';
import '/constants/colors.dart';
import '/screens/menu/categories.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'basket.dart';

class MenuPage extends StatefulWidget {
  // MenuPage({Key key}) : super(key: key);

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  double ratio;
  bool empty = false;
  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return Scaffold(
        endDrawer: Container(
          width: 424 * ratio,
          child: Drawer(
            child: BasketPage(),
          ),
        ),
        body: Row(
          children: [
            model.selectedCategory == null
                ? SizedBox()
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 25 * ratio,
                      ),
                      SizedBox(
                        height: 67 * ratio,
                        child: Center(
                          child: Text('Kategoriýalar'),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height - 92 * ratio,
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: List.generate(
                              model.categories.length,
                              (index) => GestureDetector(
                                onTap: () {
                                  if (model.selectedSubcategory != null) {
                                    model.selectSubcategory = null;
                                    model.menuNavKey.currentState.pop();
                                  }
                                  model.selectCategory =
                                      model.categories[index];
                                },
                                child: SideCategoryCard(
                                  ratio: ratio,
                                  index: index,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
            Expanded(
              child: Navigator(
                key: model.menuNavKey,
                initialRoute: 'categories',
                onGenerateRoute: (settings) {
                  Widget page;
                  switch (settings.name) {
                    case 'categories':
                      page = CategoriesPage();
                      break;
                  }
                  return PageRouteBuilder(
                    transitionDuration: Duration(milliseconds: 100),
                    reverseTransitionDuration: Duration(milliseconds: 100),
                    pageBuilder: (context, animation, animation1) =>
                        FadeTransition(opacity: animation, child: page),
                  );
                },
              ),
            ),
            Container(
              width: 1,
              color: UIColors().stroke,
            ),
            SideBasket(),
          ],
        ),
      );
    });
  }
}
