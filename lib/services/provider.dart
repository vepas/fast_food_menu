import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import '/components/widgets/basket_item_card.dart';
import '../constants/translation.dart';
import '/models/basket_item.dart';
import '/models/category.dart';
import '/models/desk.dart';
import '/models/food.dart';
import '/models/hall.dart';
import '/models/order.dart';
import '/models/orderItem.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image/image.dart' as IMG;

class MenuProvider extends ChangeNotifier {
  MenuProvider() {
    getTables();
    getDetails();
    getLocalData();
  }

  // String _apiURL = 'http://192.168.31.142:8000/api/v1';
  String _ip = 'menuly.pikir.biz';
  String _apiURL = 'http://menuly.pikir.biz/api/v1';
  String _appDir;
  bool _withImage = true;
  bool _makeOrderLoading = false;
  bool _makeOrderState = false;
  bool isSubcategoryPage = false;
  bool _foodAddAnimationState = false;
  Desk _selectedTable;
  Hall _selectedHall;
  int _imageCount = 0;
  int _selectedTableID = 0;
  int _selectedHallID = 0;
  int _storedImageCount = 0;
  int _ordersTotalCost = 0;
  Category _selectedCategory;
  int _selectedCategoryIndex;
  Category _selectedSubcategory;
  String _selectedLang = 'tm';
  List<Order> _orders = [];
  List<Hall> _halls = [];
  Map<String, dynamic> _details = {'code': '55555'};
  Map<String, dynamic> _hal = {};

  List<Category> _categories = [];
  GlobalKey<NavigatorState> menuNavKey = GlobalKey<NavigatorState>();
  GlobalKey<NavigatorState> mainNavKey = GlobalKey<NavigatorState>();
  GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();

  List<BasketItem> _basket = [];
  Map<String, dynamic> get hal => _hal;
  String get ip => _ip;

  bool get withImage => _withImage;

  bool get foodAddAnimationState => _foodAddAnimationState;

  bool get makeOrderState => _makeOrderState;

  bool get makeOrderLoading => _makeOrderLoading;

  int get storedImageCount => _storedImageCount;

  int get imageCount => _imageCount;

  int get ordersTotalCost => _ordersTotalCost;

  String get selectedLang => _selectedLang;

  Desk get selectedTable => _selectedTable;

  Hall get selectedHall => _selectedHall;

  int get selectedCategoryIndex => _selectedCategoryIndex;

  String get password => _details['code'];

  String get appDir => _appDir;

  Map<String, dynamic> get details => _details;

  Category get selectedCategory => _selectedCategory;

  Category get selectedSubcategory => _selectedSubcategory;

  // this data is taken from fake categories
  List<Category> get categories => _categories;

  List<BasketItem> get basket => _basket;

  List<Order> get orders => _orders;

  List<Hall> get halls => _halls;

  int get totalCost {
    int _totalCost = 0;

    _basket.forEach((item) {
      _totalCost += item.food.getPrice * item.quantity;
    });

    return _totalCost;
  }

  int get selectedHallIndex {
    for (int i = 0; i < _halls.length; i++) {
      Hall hall = _halls[i];
      if (hall.id == _selectedHall.id) return i;
    }
    return 0;
  }

  // set setIP(String ip) {
  //   _ip = ip;
  // }

  foodAddAnimation() {
    _foodAddAnimationState = true;
    Future.delayed(Duration(milliseconds: 400), () {
      _foodAddAnimationState = false;
      notifyListeners();
    });
  }

  selectLang(String lang) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _selectedLang = lang;
    prefs.setString('lang', lang);
  }

  selectTable(Desk table) async {
    _selectedTable = table;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('table', table.id.toString());
    _basket = [];
    _hal = _selectedHall.name;
    // print
    getOrders();
    notifyListeners();
  }

  selectHall(Hall hall) async {
    _selectedHall = hall;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('hall', hall.id.toString());
    notifyListeners();
  }

  void notify() {
    notifyListeners();
  }

  set selectCategory(Category category) {
    if (category != null) {
      _selectedCategory = category;
      _selectedCategoryIndex =
          categories.indexWhere((element) => element.id == category.id);
      isSubcategoryPage = false;
      print('saa');
    } else {
      _selectedCategory = null;
    }
    notifyListeners();
  }

  set selectSubcategory(Category subcategory) {
    if (subcategory == null) {
      _selectedSubcategory = null;
    } else {
      _selectedSubcategory = subcategory;
      isSubcategoryPage = true;
    }
    notifyListeners();
  }

  String getTranslation(String word) {
    return translations[word][_selectedLang];
  }

  toggleExpanded(int index) {
    if (orders[index].isExpanded == false)
      _orders.forEach((order) {
        order.isExpanded = false;
      });
    _orders[index].isExpanded = !orders[index].isExpanded;
  }

  String generatePrice(int price) {
    String priceS = price.toString();
    if (price == 0) return price.toString();
    if (price % 100 == 0) return priceS.substring(0, priceS.length - 2);
    return priceS.substring(0, priceS.length - 2) +
        '.' +
        priceS.substring(priceS.length - 2, priceS.length);
  }

  emptyStoredImageCount() {
    _storedImageCount = 0;
  }

  toggleWithImageState(bool state) async {
    _withImage = state;
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('withImage', state.toString());
  }

  toggleMakeOrderState(bool state) async {
    _makeOrderState = state;
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('makeOrder', state.toString());
  }

  emptyBasket() async {
    _basket = [];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('basket');
    notifyListeners();
  }

  removeItemFromList({@required BasketItem basketItem}) {
    if (listKey.currentState != null)
      listKey.currentState.removeItem(
        basket.indexOf(basketItem),
        (context, animation) => BasketItemCard(
            basketItem: basketItem, animation: animation, withRemove: false),
        duration: Duration(milliseconds: 200),
      );
  }

  insertIntoList() {
    if (listKey.currentState != null)
      listKey.currentState.insertItem(
        basket.length,
        duration: Duration(milliseconds: 200),
      );
  }

  addFoodToBasket({@required Food food}) async {
    bool contains = false;
    _basket.forEach((element) {
      if (element.food.id == food.id) {
        element.quantity++;
        contains = true;
      }
    });
    if (!contains) _basket.add(BasketItem(food: food));

    List<Map<String, dynamic>> _localBasket = _basket
        .map((item) => {
              'food': {
                'id': item.food.id,
                'name': item.food.name,
                'size': item.food.size,
                'image': item.food.image,
                'imageID': item.food.imageID,
                'price': item.food.price,
                'discountPrice': item.food.discountPrice
              },
              'quantity': item.quantity,
            })
        .toList();
    foodAddAnimation();
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('basket', json.encode(_localBasket));
  }

  reduceFoodFromBasket({@required Food food}) async {
    _basket.removeWhere(
        (element) => element.food.id == food.id && element.quantity == 1);
    _basket
        .firstWhere((element) => element.food.id == food.id,
            orElse: () => BasketItem(food: food))
        .quantity--;
    notifyListeners();
    List<Map<String, dynamic>> _localBasket = _basket
        .map((item) => {
              'food': {
                'id': item.food.id,
                'name': item.food.name,
                'size': item.food.size,
                'image': item.food.image,
                'imageID': item.food.imageID,
                'price': item.food.price,
                'discountPrice': item.food.discountPrice
              },
              'quantity': item.quantity,
            })
        .toList();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('basket', json.encode(_localBasket));
  }

  Future<bool> getLocalData() async {
    try {
      //get images directory
      var docDir = await getApplicationDocumentsDirectory();
      _appDir = docDir.path + '/images';
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.containsKey('ip')) {
        _ip = prefs.getString('ip');
        _apiURL = 'http://$_ip/api/v1';
      }
      if (prefs.containsKey('lang'))
        _selectedLang = prefs.getString('lang');
      else
        _selectedLang = 'tm';
      //get foods
      _categories = getCategoriesAsList(
          json.decode(prefs.getString('categories'))['data']);
      //get tables
      if (prefs.containsKey('table'))
        _selectedTableID = int.parse(prefs.getString('table'));
      if (prefs.containsKey('hall'))
        _selectedHallID = int.parse(prefs.getString('hall'));
      if (prefs.containsKey('tables')) {
        getTablesFromJson(json.decode(prefs.getString('tables')));
      }
      //get make order and with image states
      _makeOrderState = prefs.getString('makeOrder') == 'false' ? false : true;
      _withImage = prefs.getString('withImage') == 'false' ? false : true;
      //get app details
      _details = json.decode(prefs.getString('details'));
      //get local basket
      json.decode(prefs.getString('basket')).forEach(
        (item) {
          _basket.add(
            BasketItem(
              food: Food(
                id: item['food']['id'],
                name: item['food']['name'],
                size: item['food']['size'],
                image: item['food']['image'],
                imageID: item['food']['imageID'],
                price: item['food']['price'],
                discountPrice: item['food']['discountPrice'],
              ),
              quantity: item['quantity'],
            ),
          );
        },
      );
      //get orders
      if (_makeOrderState) getOrders();
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> changeIP(String ip) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('ip');
      prefs.setString('ip', ip);
      _ip = ip;
      _apiURL = 'http://$ip/api/v1';
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> getDetails() async {
    try {
      http.Response response = await http.get(
        '$_apiURL/details',
        headers: {
          'Content-Type': 'application/json',
        },
      );
      _details = json.decode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('details', json.encode(_details));
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<bool> getTables() async {
    try {
      http.Response response = await http.get(
        '$_apiURL/tables',
        headers: {
          'Content-Type': 'application/json',
        },
      );

      getTablesFromJson(json.decode(response.body));
      if (_selectedTable == null) {
        selectTable(_halls[0].tables[0]);
        selectHall(_halls[0]);
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('tables', response.body);
      return true;
    } catch (err) {
      return false;
    }
  }

  getTablesFromJson(Map<String, dynamic> json) {
    _halls = [];
    json['data'].forEach(
      (hall) {
        List<Desk> tables = [];
        hall['tables'].forEach((table) {
          Desk newTable = Desk(
            id: table['id'],
            number: table['number'],
            status: table['status'],
          );
          if (newTable.id == _selectedTableID) {
            _selectedTable = newTable;
          }
          tables.add(newTable);
        });
        Hall newHall = Hall(
          id: hall['id'],
          name: hall['name'],
          tables: tables,
        );
        if (newHall.id == _selectedHallID) {
          _selectedHall = newHall;
        }
        _halls.add(newHall);
      },
    );
  }

  Future<bool> getFoods(double ratio) async {
    _storedImageCount = 0;
    _categories = [];
    _imageCount = 0;
    try {
      http.Response response = await http.get('$_apiURL/foods', headers: {
        'Content-Type': 'application/json',
      });
      _categories = getCategoriesAsList(json.decode(response.body)['data']);

      json.decode(response.body)['data'].forEach(
        (category) async {
          notifyListeners();
          category['subcategories'].forEach((subcategory) async {
            await storeImage(
              ratio: ratio,
              url: subcategory['image'],
              isFood: false,
              id: subcategory['id'],
            );
            _storedImageCount++;
            notifyListeners();
            subcategory['foods'].forEach((food) async {
              await storeImage(
                ratio: ratio,
                url: food['image'],
                id: food['id'],
              );
              _storedImageCount++;
              notifyListeners();
            });
          });
          category['foods'].forEach(
            (food) async {
              await storeImage(
                ratio: ratio,
                url: food['image'],
                id: food['id'],
              );
              _storedImageCount++;
              notifyListeners();
            },
          );
          await storeImage(
            ratio: ratio,
            url: category['image'],
            isFood: false,
            id: category['id'],
          );
          _storedImageCount++;
        },
      );
      var docDir = await getApplicationDocumentsDirectory();
      _appDir = docDir.path + '/images';
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('categories');
      prefs.setString('categories', response.body);
      prefs.remove('appDir');
      prefs.setString('appDir', _appDir);
      notifyListeners();
      return true;
    } catch (err) {
      return false;
    }
  }

  List<Category> getCategoriesAsList(List<dynamic> data, {int parentID = 0}) {
    List<Category> categories = [];
    data.forEach(
      (category) {
        List<Food> foods = [];
        _imageCount++;
        category['foods'].forEach((food) {
          _imageCount++;
          food['sizes'].forEach((size) {
            foods.add(Food.fromJson(food: food, size: size));
          });
        });
        categories.add(
          Category(
            id: category['id'],
            parentId: parentID,
            image: category['image'],
            name: category['name'],
            isDrink: category['is_drink'],
            subcategories: getCategoriesAsList(category['subcategories'],
                parentID: category['id']),
            foods: foods,
          ),
        );
      },
    );
    return categories;
  }

  Future<String> storeImage({
    @required String url,
    double ratio,
    bool isFood = true,
    @required int id,
  }) async {
    if (isFood == false) {}
    try {
      var response = await http.get(url);
      var documentDirectory = await getApplicationDocumentsDirectory();
      var firstPath = documentDirectory.path +
          (isFood ? '/images/foods' : '/images/categories');
      var filePathAndName = documentDirectory.path +
          (isFood ? '/images/foods/$id.jpg' : '/images/categories/$id.jpg');
      await Directory(firstPath).create(recursive: true);
      File file = new File(filePathAndName);
      File file1 = new File(documentDirectory.path +
          (isFood ? '/images/foods/q$id.jpg' : '/images/categories/q$id.jpg'));

      file.writeAsBytesSync(response.bodyBytes);
      file1.writeAsBytesSync(resizeImage(
          data: response.bodyBytes,
          targetHeigh: ((isFood ? 170 : 350) * ratio).toInt(),
          targetWidth: ((isFood ? 170 : 350) * ratio).toInt()));
      notifyListeners();
      return filePathAndName;
    } catch (err) {
      throw Exception();
    }
  }

  Uint8List resizeImage({Uint8List data, int targetWidth, int targetHeigh}) {
    Uint8List resizedData = data;
    IMG.Image img = IMG.decodeImage(data);
    IMG.Image resized = IMG.copyResize(img, height: targetHeigh);
    resizedData = IMG.encodeJpg(resized);
    return resizedData;
  }

  Future<bool> makeOrder() async {
    print('inner');
    _makeOrderLoading = true;
    notifyListeners();
    List<Map<String, dynamic>> items = _basket
        .map((item) => {'size_id': item.food.id, 'quantity': item.quantity})
        .toList();
    try {
      await http.post('$_apiURL/orders',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
          body: json.encode(
            {'items': items, 'table_id': _selectedTable.id},
          ));
      print('inner2');

      await emptyBasket();
      await getOrders();
      await getTables();
      _makeOrderLoading = false;
      notifyListeners();
      return true;
    } catch (err) {
      _makeOrderLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> getOrders() async {
    try {
      http.Response response = await http
          .get('$_apiURL/orders/table/${selectedTable.id}', headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      });
      _orders = [];
      _ordersTotalCost = 0;
      json.decode(response.body)['data'].forEach((order) {
        List<OrderItem> items = [];
        order['items'].forEach((item) {
          items.add(
            OrderItem(
              name: item['name'],
              size: item['size'],
              quantity: item['quantity'],
              cost: item['cost'],
            ),
          );
        });
        _orders.add(
          Order(
              code: order['code'],
              createdAt: order['created_at'],
              cost: order['cost'],
              serviceCost: order['service_cost'],
              totalCost: order['total_cost'],
              items: items),
        );
        _ordersTotalCost += order['total_cost'];
      });
      notifyListeners();
      return true;
    } catch (err) {
      return false;
    }
  }
}
