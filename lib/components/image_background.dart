import 'package:flutter/material.dart';

class ImageBackground extends StatelessWidget {
  final Widget child;

  const ImageBackground({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        padding: EdgeInsets.all(11),
        decoration: BoxDecoration(
          color: Color(0xFF1A1D22).withOpacity(0.7),
        ),
        child: Center(child: child),
      ),
    );
  }
}
