import 'package:fast_food_menu/constants/colors.dart';

import '/components/widgets/confirm_button.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:ui' as ui;

import 'package:provider/provider.dart';

class OrderMadePopup extends StatelessWidget {
  const OrderMadePopup({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    MenuProvider model = Provider.of<MenuProvider>(context);
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      backgroundColor: Colors.transparent,
      content: Container(
        width: 468 * ratio,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Color(0xFF1F2328),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              height: 52 * ratio,
              padding: EdgeInsets.only(left: 26, right: 5),
              decoration: BoxDecoration(
                color: Color(0xFF2C3137),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    model.getTranslation('Sent'),
                    style: TextStyle(
                      fontSize: 16 * ratio,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            ShaderMask(
              blendMode: BlendMode.srcIn,
              shaderCallback: (shader) {
                return ui.Gradient.linear(Offset(0, 0), Offset(45, 0), [
                  UIColors().primary,
                  UIColors().primary,
                ]);
              },
              child: SvgPicture.asset(
                'assets/icons/checked 1.svg',
                height: 45 * ratio,
                color: UIColors().primary,
              ),
            ),
            SizedBox(height: 30),
            UIText(
              context: context,
              text: model.getTranslation(
                  'Order is sent to kitchen and will be ready soon'),
              align: TextAlign.center,
            ).bodyText2,
            SizedBox(height: 25),
            ConfirmButton(
              text: model.getTranslation('Continue'),
              horizontalPadding: 129,
              onTap: () {
                Navigator.pop(context);
              },
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
