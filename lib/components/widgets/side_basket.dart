import 'dart:io';

import '/constants/colors.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class SideBasket extends StatelessWidget {
  const SideBasket({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;

    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        return GestureDetector(
          onTap: () {
            Scaffold.of(context).openEndDrawer();
          },
          child: model.withImage
              ? Container(
                  width: 125 * ratio,
                  decoration: BoxDecoration(
                    color: UIColors().cartBackground,
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 40,
                        child: Center(
                          child: SvgPicture.asset(
                            'assets/icons/double_arrow.svg',
                            height: 32,
                          ),
                        ),
                      ),
                      Container(
                        width: 1,
                        color: UIColors().stroke,
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            SizedBox(height: 15),
                            Container(
                              height: 63 * ratio,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 15),
                                  UIText(
                                          context: context,
                                          text: model.getTranslation('Basket'))
                                      .bodyTextPassive2,
                                  // SizedBox(height: 3),
                                  UIText(
                                          context: context,
                                          text: model.getTranslation('Quant.') +
                                              ': ${model.basket.length}')
                                      .bodyTextPassive3,
                                ],
                              ),
                            ),
                            Container(
                              height: 1,
                              color: UIColors().stroke,
                            ),
                            Expanded(
                              child: model.basket.length > 0
                                  ? ListView(
                                      padding: EdgeInsets.only(top: 20),
                                      children:
                                          _buildBasketImages(model, ratio),
                                    )
                                  : Center(
                                      child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        SvgPicture.asset(
                                          'assets/icons/No food Icon.svg',
                                          height: 48 * ratio,
                                        ),
                                        SizedBox(height: 5),
                                        UIText(
                                          context: context,
                                          align: TextAlign.center,
                                          text: model
                                              .getTranslation('Empty basket'),
                                        ).bodyTextPassive3
                                      ],
                                    )),
                            ),
                            SizedBox(
                                height:
                                    model.basket.length > 0 ? 0 : 73 * ratio),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  width: 50,
                  decoration: BoxDecoration(
                    color: UIColors().cartBackground,
                  ),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 50,
                          margin: EdgeInsets.only(bottom: 40),
                          decoration: BoxDecoration(
                            border: Border.symmetric(
                                horizontal:
                                    BorderSide(color: UIColors().stroke)),
                          ),
                          child: Center(
                            child: AnimatedDefaultTextStyle(
                              duration: Duration(milliseconds: 200),
                              style: model.foodAddAnimationState
                                  ? TextStyle(
                                      color: UIColors().primary,
                                      fontSize: 24 * ratio,
                                      fontWeight: FontWeight.w700,
                                      shadows: [
                                          Shadow(
                                              color: UIColors().primary,
                                              blurRadius: 3),
                                        ])
                                  : TextStyle(
                                      color: UIColors().passive,
                                      fontSize: 16 * ratio,
                                      fontWeight: FontWeight.w500,
                                    ),
                              child: Text(model.basket.length.toString()),
                            ),
                          ),
                        ),
                        SvgPicture.asset(
                          'assets/icons/double_arrow.svg',
                          height: 32,
                        ),
                        SizedBox(
                          height: 90,
                        ),
                      ],
                    ),
                  ),
                ),
        );
      },
    );
  }

  List _buildBasketImages(MenuProvider model, double ratio) {
    return model.basket
        .map(
          (item) => Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Center(
              child: SizedBox(
                height: 45 * ratio,
                width: 45 * ratio,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.file(
                    File('${model.appDir}/foods/q${item.food.imageID}.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
        )
        .toList();
  }
}
