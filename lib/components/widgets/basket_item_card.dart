import 'dart:io';

import '/constants/colors.dart';
import '/constants/text.dart';

import '/models/basket_item.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class BasketItemCard extends StatelessWidget {
  final BasketItem basketItem;
  final Animation<double> animation;
  final bool withRemove;

  BasketItemCard(
      {@required this.basketItem,
      @required this.animation,
      this.withRemove = true});

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return SizeTransition(
        sizeFactor: animation,
        child: Container(
          height: 45 * ratio,
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // model.withImage
              //     ? Container(
              //         width: 45 * ratio,
              //         height: 45 * ratio,
              //         margin: EdgeInsets.only(right: 10),
              //         child: ClipRRect(
              //           child: Image.file(
              //             File(
              //                 '${model.appDir}/foods/${basketItem.food.imageID}.jpg'),
              //             fit: BoxFit.cover,
              //           ),
              //           borderRadius: BorderRadius.circular(8),
              //         ),
              //       )
              //     : SizedBox(),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UIText(
                            context: context,
                            text: basketItem.food
                                .foodName(model.selectedLang, summarize: 18))
                        .foodNameDemo,
                    UIText(
                            context: context,
                            text:
                                '${model.generatePrice((basketItem.food.getPrice))} TMT')
                        .bodyText4,
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    onTap: () {
                      if (withRemove) {
                        if (basketItem.quantity == 1)
                          model.removeItemFromList(basketItem: basketItem);
                        model.reduceFoodFromBasket(food: basketItem.food);
                      }
                    },
                    child: Container(
                      height: 45 * ratio,
                      width: 45 * ratio,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Color(0xff29292B),

                        // border: Border.all(color: UIColors().primary),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: basketItem.quantity == 1
                          ? SvgPicture.asset(
                              'assets/icons/Delete Icon.svg',
                              height: 20 * ratio,
                            )
                          : Icon(
                              Icons.remove,
                              color: Colors.white,
                            ),
                    ),
                  ),
                  SizedBox(
                    width: 36 * ratio,
                    child: Center(child: Text(basketItem.quantity.toString())),
                  ),
                  InkWell(
                    onTap: () {
                      model.addFoodToBasket(food: basketItem.food);
                    },
                    child: Container(
                      height: 45 * ratio,
                      width: 45 * ratio,
                      decoration: BoxDecoration(
                        color: Color(0xff29292B),
                        // border: Border.all(color: UIColors().primary),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
