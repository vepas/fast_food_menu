import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/category.dart';
import '/screens/menu/subcategories.dart';
import '/screens/menu/foods.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryCardWithoutImage extends StatelessWidget {
  final Category category;

  CategoryCardWithoutImage({Key key, @required this.category})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return GestureDetector(
        onTap: () {
          if (category.parentId != 0) {
            model.selectSubcategory = category;
            model.menuNavKey.currentState.push(PageRouteBuilder(
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
              pageBuilder: (context, animation, animation1) =>
                  SubcategoryPage(),
            ));
          } else {
            model.selectCategory = category;
            model.menuNavKey.currentState.push(PageRouteBuilder(
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
              pageBuilder: (context, animation, animation1) => CategoryPage(),
            ));
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: UIColors().stroke,
          ),
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.only(right: 15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                UIText(
                  context: context,
                  text: category.name[model.selectedLang],
                ).foodName,
                Container(
                    margin: EdgeInsets.only(top: 14),
                    height: 3,
                    width: 20,
                    color: UIColors().primary),
              ],
            ),
          ),
        ),
      );
    });
  }
}
