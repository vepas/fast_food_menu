import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/basket_item.dart';
import '/models/category.dart';
import '/models/food.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FoodPopup extends StatefulWidget {
  final Food food;

  FoodPopup({Key key, @required this.food}) : super(key: key);

  @override
  _FoodPopupState createState() => _FoodPopupState();
}

class _FoodPopupState extends State<FoodPopup> {
  CarouselControllerImpl controller;
  double ratio;
  int index;

  @override
  void initState() {
    super.initState();
    controller = CarouselControllerImpl();
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      if (model.isSubcategoryPage) {
        index = model.selectedSubcategory.foods.indexOf(widget.food);
      } else {
        index = model.selectedCategory.foods.indexOf(widget.food);
      }
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        contentPadding: EdgeInsets.zero,
        insetPadding: EdgeInsets.zero,
        backgroundColor: Colors.transparent,
        content: Container(
          width: MediaQuery.of(context).size.width,
          height: 607 * ratio,
          child: CarouselSlider(
            options: CarouselOptions(
              height: 553 * ratio,
              aspectRatio: 500 / 607,
              initialPage: index,
              viewportFraction: 0.52,
              enlargeStrategy: CenterPageEnlargeStrategy.scale,
              // enlargeCenterPage: true,
            ),
            carouselController: controller,
            items: _buildCarouselItems(),
          ),
        ),
      );
    });
  }

  List<Widget> _buildCarouselItems() {
    List<Widget> items = [];
    MenuProvider model = Provider.of<MenuProvider>(context, listen: false);
    Category currentCategory = model.selectedCategory;
    if (model.isSubcategoryPage) currentCategory = model.selectedSubcategory;

    currentCategory.foods.forEach((food) {
      BasketItem basketItem = BasketItem(quantity: 0);
      model.basket.forEach((element) {
        if (element.food.id == food.id) basketItem = element;
      });
      items.add(_carouselItem(context, basketItem, food));
    });
    return items;
  }

  Widget _carouselItem(BuildContext context, BasketItem basketItem, Food food) {
    MenuProvider model = Provider.of<MenuProvider>(context, listen: false);
    return Container(
      width: 500 * ratio,

      // height: 463 * ratio,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: UIColors().cartBackground,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          model.withImage
              ? Container(
                  alignment: Alignment.topRight,
                  height: 439 * ratio,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)),
                    image: DecorationImage(
                      image: FileImage(
                          File('${model.appDir}/foods/${food.imageID}.jpg')),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(top: 5 * ratio, right: 5 * ratio),
                    height: 40 * ratio,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black.withOpacity(.4)),
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.clear,
                        size: 22 * ratio,
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          Expanded(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: SizedBox(
                  width: 400 * ratio,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 160 * ratio,
                            child: UIText(
                              context: context,
                              text: food.name[model.selectedLang] +
                                  (food.size != null
                                      ? ', ${food.size[model.selectedLang]}'
                                      : ''),
                            ).foodName,
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              UIText(
                                context: context,
                                text:
                                    '${model.generatePrice(food.getPrice)} TMT',
                              ).bodyText3,
                              food.discountPrice != null
                                  ? Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: UIText(
                                              context: context,
                                              text:
                                                  '${model.generatePrice(food.price)} TMT')
                                          .discountPrice,
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ],
                      ),
                      AnimatedSwitcher(
                        duration: Duration(milliseconds: 300),
                        child: basketItem.quantity == 0
                            ? Container(
                                key: ValueKey(0),
                                width: 240 * ratio,
                                alignment: Alignment.centerRight,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        if (model.basket.length > 0)
                                          model.insertIntoList();
                                        model.addFoodToBasket(food: food);
                                      },
                                      child: Container(
                                        height: 45 * ratio,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 30),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          color: UIColors().primary,
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Text(
                                          model.getTranslation('Add to basket'),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                key: ValueKey(1),
                                width: 240 * ratio,
                                alignment: Alignment.centerRight,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        if (basketItem.quantity == 1)
                                          model.removeItemFromList(
                                              basketItem: basketItem);
                                        model.reduceFoodFromBasket(food: food);
                                      },
                                      child: Container(
                                        height: 45 * ratio,
                                        width: 45 * ratio,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: UIColors().primary),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Icon(
                                          Icons.remove,
                                          color: UIColors().primary,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: 70 * ratio,
                                      alignment: Alignment.center,
                                      child: UIText(
                                        context: context,
                                        text: basketItem.quantity.toString(),
                                      ).h2,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        model.addFoodToBasket(food: food);
                                      },
                                      child: Container(
                                        height: 45 * ratio,
                                        width: 45 * ratio,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              color: UIColors().primary),
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: Icon(
                                          Icons.add,
                                          color: UIColors().primary,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
