import 'dart:io';

import '/components/widgets/drink_popup.dart';
import '/components/widgets/food_popup.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/food.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FoodCard extends StatelessWidget {
  final Food food;
  final bool isDrink;
  FoodCard({Key key, @required this.food, this.isDrink}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      bool added = false;
      model.basket.forEach((element) {
        if (element.food.id == food.id) {
          added = true;
        }
      });
      print(food.imageID);
      return GestureDetector(
        onTap: () {
          showDialog(
            context: context,
            builder: (context) {
              return FoodPopup(food: food);
            },
          );
        },
        child: Container(
          decoration: BoxDecoration(
            border: added
                ? Border.all(width: .5, color: UIColors().primary)
                : Border(),
            borderRadius: BorderRadius.circular(8),
            color: Color(0xff29292B),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 198 * ratio),
                      // padding: EdgeInsets.only(right: 25),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: 15 * ratio, top: 15 * ratio),
                            child: UIText(
                              context: context,
                              text: food.name[model.selectedLang] +
                                  (food.size != null
                                      ? ', ${food.size[model.selectedLang]}'
                                      : ''),
                            ).foodName,
                          ),
                        ],
                      ),
                    ),
                  ),
                  AnimatedSwitcher(
                    duration: Duration(milliseconds: 100),
                    child: added
                        ? Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              width: 35 * ratio,
                              height: 35 * ratio,
                              decoration: BoxDecoration(
                                color: UIColors().selectedFood,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(8),
                                  bottomLeft: Radius.circular(8),
                                ),
                              ),
                              child: Center(
                                  child: Icon(
                                Icons.check,
                                color: Colors.black,
                              )),
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(15 * ratio),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        UIText(
                                context: context,
                                text: food.discountPrice != null
                                    ? '${model.generatePrice(food.price)} TMT'
                                    : '')
                            .discountPrice,
                        SizedBox(width: 13 * ratio),
                        UIText(
                                color: Color(0xffFED931),
                                context: context,
                                text:
                                    '${model.generatePrice(food.getPrice)} TMT')
                            .bodyText3,
                      ],
                    ),
                    Container(
                      height: 90 * ratio,
                      width: 90 * ratio,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.file(
                          File('${model.appDir}/foods/q${food.imageID}.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        /* ), */
      );
    });
  }
}
