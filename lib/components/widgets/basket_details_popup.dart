import '/components/widgets/order_made_popup.dart';
import '/constants/colors.dart';
import '/constants/text.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BasketDetailsPopup extends StatelessWidget {
  const BasketDetailsPopup({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return AlertDialog(
        contentPadding: EdgeInsets.zero,
        backgroundColor: Colors.transparent,
        content: Container(
          width: 360 * ratio,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Color(0xFF1F2328),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                height: 52 * ratio,
                padding: EdgeInsets.only(left: 26, right: 5),
                decoration: BoxDecoration(
                  color: Color(0xFF2C3137),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      model.getTranslation('Show order to waiter'),
                      style: TextStyle(
                        fontSize: 16 * ratio,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
                child: Column(
                  children: [
                    Row(
                      children: [
                        UIText(
                                context: context,
                                text: model.getTranslation('Meal'))
                            .smallTextPassive,
                        Expanded(
                          child: Container(),
                        ),
                        UIText(
                                context: context,
                                text: model.getTranslation('Quantity'))
                            .smallTextPassive,
                        Container(
                          width: 80 * ratio,
                          alignment: Alignment.centerRight,
                          child: UIText(
                                  context: context,
                                  text: model.getTranslation('Price'))
                              .smallTextPassive,
                        ),
                      ],
                    ),
                    Container(
                        height: 1,
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 15),
                        color: UIColors().stroke),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: 260 * ratio,
                      ),
                      child: RawScrollbar(
                        isAlwaysShown: true,
                        thickness: 5 * ratio,
                        radius: Radius.circular(8),
                        thumbColor: UIColors().primary,
                        child: SingleChildScrollView(
                          padding: EdgeInsets.only(
                              bottom: 15, top: 15, right: 12 * ratio),
                          child: Column(
                            children: List.generate(
                              model.basket.length,
                              (index) => Column(
                                children: [
                                  Row(
                                    children: [
                                      UIText(
                                        context: context,
                                        text: model.basket[index].food
                                            .name[model.selectedLang],
                                      ).bodyText2,
                                      Expanded(
                                        child: Container(),
                                      ),
                                      UIText(
                                        context: context,
                                        text: model.basket[index].quantity
                                            .toString(),
                                      ).bodyText2,
                                      Container(
                                        width: 80 * ratio,
                                        alignment: Alignment.centerRight,
                                        child: UIText(
                                                context: context,
                                                text: model.generatePrice(model
                                                        .basket[index]
                                                        .food
                                                        .getPrice) +
                                                    ' TMT')
                                            .bodyText2,
                                      ),
                                    ],
                                  ),
                                  index < model.basket.length - 1
                                      ? Container(
                                          height: 1,
                                          width: double.infinity,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 15),
                                          color: UIColors().stroke,
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 1,
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 15),
                      color: UIColors().stroke,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        UIText(
                                context: context,
                                text: model.getTranslation('Cost'),
                                color: UIColors().passive)
                            .bodyText2,
                        UIText(
                                context: context,
                                text:
                                    '${model.generatePrice(model.totalCost)} TMT')
                            .bodyText2,
                      ],
                    ),
                    // : SizedBox(),
                    SizedBox(height: 8),
                    model.details['service']['cost'] > 0
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              UIText(
                                      context: context,
                                      text: model.getTranslation('Service') +
                                          ' ' +
                                          (model.details['service']['unit'] ==
                                                  'manat'
                                              ? ''
                                              : '(${model.details['service']['cost']}%)'),
                                      color: UIColors().passive)
                                  .bodyText2,
                              UIText(
                                context: context,
                                text: model.details['service']['unit'] ==
                                        'manat'
                                    ? '+${model.generatePrice(model.details['service']['cost'])} TMT'
                                    : '+${model.generatePrice((model.totalCost * model.details['service']['cost'] / 100).round())} TMT',
                              ).bodyText2,
                            ],
                          )
                        : SizedBox(),
                    SizedBox(
                        height: model.details['service']['cost'] > 0 ? 8 : 0),
                    Container(
                        height: 1,
                        width: 218 * ratio,
                        margin: EdgeInsets.symmetric(vertical: 10),
                        color: UIColors().stroke),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        UIText(
                                context: context,
                                text: model.getTranslation('Total Cost'),
                                color: UIColors().passive)
                            .foodNameDemo,
                        UIText(
                                context: context,
                                text:
                                    '${model.generatePrice(model.totalCost + (model.details['service']['unit'] == 'manat' ? model.details['service']['cost'] : (model.totalCost * model.details['service']['cost'] / 100).round()))} TMT')
                            .foodNameDemo,
                      ],
                    ),
                    SizedBox(height: 30),
                    InkWell(
                      onTap: () async {
                        if (model.makeOrderState) {
                          if (await model.makeOrder() == true) {
                            Navigator.pop(context);
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return OrderMadePopup();
                                });
                          }
                        } else {
                          Navigator.pop(context);
                        }
                      },
                      child: Container(
                        width: double.infinity,
                        height: 47 * ratio,
                        padding: EdgeInsets.symmetric(vertical: 11),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: UIColors().primary),
                        child: Center(
                          child: model.makeOrderLoading
                              ? SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 3,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.black),
                                  ),
                                )
                              : Center(
                                  child: Text(
                                    model.getTranslation(model.makeOrderState
                                        ? 'Make order'
                                        : 'Continue'),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
