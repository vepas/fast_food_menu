import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/basket_item.dart';
import '/models/food.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FoodPopup extends StatelessWidget {
  final Food food;
  const FoodPopup({Key key, @required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BasketItem basketItem = BasketItem(quantity: 0);

    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      model.basket.forEach((element) {
        if (element.food.id == food.id) basketItem = element;
      });
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        contentPadding: EdgeInsets.zero,
        backgroundColor: UIColors().cartBackground,
        content: Container(
          width: 550 * ratio,
          // height: 463 * ratio,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.topRight,
                height: 285 * ratio,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                    image: NetworkImage(food.image),
                    fit: BoxFit.cover,
                  ),
                ),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.clear),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(30 * ratio),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIText(
                                    context: context,
                                    text: food.name[model.selectedLang])
                                .foodName,
                            SizedBox(height: 5),
                            Row(
                              children: [
                                UIText(
                                        context: context,
                                        text:
                                            '${model.generatePrice(food.getPrice)} TMT')
                                    .bodyText3,
                                food.discountPrice != null
                                    ? Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: UIText(
                                                context: context,
                                                text:
                                                    '${model.generatePrice(food.price)} TMT')
                                            .discountPrice,
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ],
                        ),
                        basketItem.quantity == 0
                            ? InkWell(
                                onTap: () {
                                  if (model.basket.length > 0)
                                    model.insertIntoList();
                                  model.addFoodToBasket(food: food);
                                },
                                child: Container(
                                  height: 45 * ratio,
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.symmetric(horizontal: 31),
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFFF89B32),
                                          Color(0xFFFC542D),
                                        ],
                                      ),
                                      borderRadius: BorderRadius.circular(8),
                                      boxShadow: [
                                        BoxShadow(
                                          offset: Offset(0, 8),
                                          blurRadius: 24,
                                          color: Color(0xFFEA7C69)
                                              .withOpacity(0.3),
                                        )
                                      ]),
                                  child: Text(
                                    model.getTranslation('Add to basket'),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              )
                            : Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (basketItem.quantity == 1)
                                        model.removeItemFromList(
                                            basketItem: basketItem);
                                      model.reduceFoodFromBasket(food: food);
                                    },
                                    child: Container(
                                      height: 45 * ratio,
                                      width: 45 * ratio,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: UIColors().primary),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Icon(
                                        Icons.remove,
                                        color: UIColors().primary,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 30),
                                    child: UIText(
                                      context: context,
                                      text: basketItem.quantity.toString(),
                                    ).h2,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      model.addFoodToBasket(food: food);
                                    },
                                    child: Container(
                                      height: 45 * ratio,
                                      width: 45 * ratio,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: UIColors().primary),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        color: UIColors().primary,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
