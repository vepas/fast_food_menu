import 'dart:async';

import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/desk.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class TableCard extends StatefulWidget {
  final Desk table;
  TableCard({Key key, this.table}) : super(key: key);

  @override
  _TableCardState createState() => _TableCardState();
}

class _TableCardState extends State<TableCard> {
  bool tapped = false;

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        return InkWell(
          onTap: () {
            setState(() {
              tapped = true;
            });
            Timer(Duration(milliseconds: 400), () {
              model.selectTable(widget.table);
              
              Navigator.pop(context);
            });
            // print(widget.table.id);
            // print(widget.table.status);
          },
          child: AnimatedPadding(
            duration: Duration(milliseconds: 300),
            padding: tapped ? EdgeInsets.all(16) : EdgeInsets.all(0),
            child: Container(
              height: 120 * ratio,
              width: 194 * ratio,
              decoration: BoxDecoration(
                color: UIColors().foodCardBackground,
                borderRadius: BorderRadius.circular(16),
                border: model.selectedTable == widget.table
                    ? Border.all(color: Color(0xfffed931))
                    : Border(),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Center(
                      child: UIText(
                        context: context,
                        text: widget.table.number < 10
                            ? '0' + widget.table.number.toString()
                            : widget.table.number.toString(),
                      ).big,
                    ),
                  ),
                  Expanded(
                    child: widget.table.status != 'empty'
                        ? Container(
                            height: 120 * ratio,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Color(0xfffed931),
                              // gradient: UIColors().mainGradient,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8),
                                bottomRight: Radius.circular(8),
                              ),
                            ),
                            child: SvgPicture.asset(
                              'assets/icons/History Clear.svg',
                              color: Colors.black,
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
