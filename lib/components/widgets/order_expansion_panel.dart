import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/order.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderExpansionPanel extends StatefulWidget {
  final int index;

  OrderExpansionPanel(this.index, {Key key}) : super(key: key);

  @override
  _OrderExpansionPanelState createState() => _OrderExpansionPanelState();
}

class _OrderExpansionPanelState extends State<OrderExpansionPanel>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;
  double ratio;

  @override
  void initState() {
    _controller = AnimationController(
      upperBound: 0.5,
      lowerBound: 0,
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      Order order = model.orders[widget.index];
      return Column(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                model.toggleExpanded(widget.index);
                model.orders[widget.index].isExpanded
                    ? _controller.forward()
                    : _controller.reverse();
              });
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIText(context: context, text: order.code).foodNameDemo,
                      UIText(context: context, text: order.createdAt)
                          .bodyTextPassive3,
                    ],
                  ),
                  RotationTransition(
                      turns: _animation,
                      child: Icon(
                        Icons.expand_more_rounded,
                        color: UIColors().passive,
                      )),
                ],
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: Container(height: 0),
            secondChild: Container(
              child: Column(
                children: [
                  Container(
                      height: 1,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 17),
                      color: UIColors().stroke),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 17),
                    child: Column(
                      children: List.generate(
                        order.items.length,
                        (index) => Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(top: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              UIText(
                                context: context,
                                text: order
                                        .items[index].name[model.selectedLang] +
                                    (order.items[index].size == null
                                        ? ''
                                        : ', ' +
                                            order.items[index]
                                                .size[model.selectedLang]),
                                color: Colors.white,
                              ).bodyTextPassive2,
                              UIText(
                                      context: context,
                                      text: model.generatePrice(
                                              order.items[index].cost) +
                                          ' TMT - ' +
                                          order.items[index].quantity
                                              .toString() +
                                          ' ' +
                                          model.getTranslation('pc.'))
                                  .bodyTextPassive3,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                      height: 1,
                      width: double.infinity,
                      margin: EdgeInsets.only(
                          top: 37, bottom: 7, left: 17, right: 17),
                      color: UIColors().stroke),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 17),
                    child: Column(
                      children: [
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            UIText(
                                    context: context,
                                    text: model.getTranslation('Cost'),
                                    color: UIColors().passive)
                                .bodyText2,
                            UIText(
                                    context: context,
                                    text: model.generatePrice(order.cost) +
                                        ' TMT')
                                .bodyText2,
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            UIText(
                                    context: context,
                                    text: model.getTranslation('Service') + ' ',
                                    color: UIColors().passive)
                                .bodyText2,
                            UIText(
                                    context: context,
                                    text:
                                        model.generatePrice(order.serviceCost) +
                                            ' TMT')
                                .bodyText2,
                          ],
                        ),
                        SizedBox(height: 8),
                        Container(
                            height: 1,
                            width: 218 * ratio,
                            margin: EdgeInsets.symmetric(vertical: 10),
                            color: UIColors().stroke),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            UIText(
                                    context: context,
                                    text: model.getTranslation('Total Cost'),
                                    color: UIColors().passive)
                                .foodNameDemo,
                            UIText(
                                    context: context,
                                    color: UIColors().primary,
                                    text: model.generatePrice(order.totalCost) +
                                        ' TMT')
                                .foodNameDemo,
                          ],
                        ),
                        SizedBox(height: 43),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            crossFadeState: model.orders[widget.index].isExpanded
                ? CrossFadeState.showSecond
                : CrossFadeState.showFirst,
            duration: Duration(milliseconds: 300),
          ),
          Container(
              height: 1,
              width: double.infinity,
              // margin: EdgeInsets.only(top: 18),
              color: UIColors().stroke),
        ],
      );
    });
  }
}
