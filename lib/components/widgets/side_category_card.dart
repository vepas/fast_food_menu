import 'dart:io';

import 'package:fast_food_menu/constants/colors.dart';
import 'package:fast_food_menu/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SideCategoryCard extends StatelessWidget {
  const SideCategoryCard({
    Key key,
    @required this.ratio,
    @required this.index,
  }) : super(key: key);

  final double ratio;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Consumer<MenuProvider>(
        builder: (context, model, child) => Container(
              margin: EdgeInsets.only(
                bottom: 28 * ratio,
                left: 28 * ratio,
                right: 28 * ratio,
              ),
              decoration: BoxDecoration(
                border: model.selectedCategoryIndex == index
                    ? Border.all(color: UIColors().primary, width: 1)
                    : Border(),
                color: UIColors().cartBackground,
                borderRadius: BorderRadius.circular(8),
              ),
              width: 102 * ratio,
              height: 135 * ratio,
              child: Column(
                children: [
                  SizedBox(
                    height: 8 * ratio,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      image: new DecorationImage(
                        image: new FileImage(
                          new File(
                              '${model.appDir}/categories/${model.categories[index].id}.jpg'),
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    height: 85 * ratio,
                    width: 85 * ratio,
                    padding: EdgeInsets.all(8 * ratio),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        (model.categories[index].name[model.selectedLang])
                                    .length >
                                12
                            ? (model.categories[index].name[model.selectedLang])
                                    .substring(0, 12) +
                                '...'
                            : model.categories[index].name[model.selectedLang],
                        textAlign: TextAlign.center,
                        style: TextStyle(),
                      ),
                    ),
                  )
                ],
              ),
            ));
  }
}
