import '/constants/colors.dart';
import '/constants/text.dart';
import '/models/food.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FoodCardWithoutImage extends StatelessWidget {
  final Food food;
  FoodCardWithoutImage({Key key, @required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        bool added = false;
        model.basket.forEach((element) {
          if (element.food.id == food.id) added = true;
        });
        return GestureDetector(
          onTap: () {
            model.addFoodToBasket(food: food);
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: UIColors().stroke,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 100),
                    child: added
                        ? Container(
                            width: 79 * ratio,
                            height: 28 * ratio,
                            decoration: BoxDecoration(
                              color: UIColors().selectedFood,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                bottomRight: Radius.circular(8),
                              ),
                            ),
                            child: Center(
                              child: UIText(
                                      context: context,
                                      text: model.getTranslation('Selected'))
                                  .smallText,
                            ),
                          )
                        : Container(),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        UIText(
                                context: context,
                                text: food.name[model.selectedLang])
                            .foodName,
                        SizedBox(height: 5),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            UIText(
                                    context: context,
                                    text: food.discountPrice != null
                                        ? '${model.generatePrice(food.price)} TMT'
                                        : '')
                                .discountPrice,
                            SizedBox(width: 13 * ratio),
                            UIText(
                                    context: context,
                                    text:
                                        '${model.generatePrice(food.getPrice)} TMT')
                                .bodyText3,
                          ],
                        ),
                        SizedBox(height: 12),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
