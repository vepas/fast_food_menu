import 'package:flutter/material.dart';

class ConfirmButton extends StatelessWidget {
  final Function onTap;
  final String text;
  final double horizontalPadding;
  const ConfirmButton(
      {Key key, this.onTap, @required this.text, this.horizontalPadding = 132})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding:
            EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: 14),
        decoration: BoxDecoration(
          // gradient: LinearGradient(
          //   colors: [
          //     Color(0xFFF89B32),
          //     Color(0xFFFC542D),
          //   ],
          // ),
          color: Color(0xfffed931),
          borderRadius: BorderRadius.circular(8),
          /*  boxShadow: [
              BoxShadow(
                offset: Offset(0, 8),
                blurRadius: 24,
                color: Color(0xFFEA7C69).withOpacity(0.3),
              )
            ] */
        ),
        child: Text(
          text,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
