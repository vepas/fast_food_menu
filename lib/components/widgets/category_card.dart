import 'dart:io';

import '/constants/colors.dart';
import '/constants/text.dart';
// import '/models/category.dart';
import '/screens/menu/subcategories.dart';
import '/screens/menu/foods.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryCard extends StatelessWidget {
  final category;

  CategoryCard({Key key, @required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Consumer<MenuProvider>(builder: (context, model, child) {
      return GestureDetector(
        onTap: () {
          if (category.parentId != 0) {
            model.selectSubcategory = category;
            model.menuNavKey.currentState.push(PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 100),
              reverseTransitionDuration: Duration(milliseconds: 100),
              pageBuilder: (context, animation, animation1) => FadeTransition(
                opacity: animation,
                child: SubcategoryPage(isDrink: category.isDrink),
              ),
            ));
          } else {
            model.selectCategory = category;
            model.menuNavKey.currentState.push(PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 100),
              reverseTransitionDuration: Duration(milliseconds: 100),
              pageBuilder: (context, animation, animation1) =>
                  FadeTransition(opacity: animation, child: CategoryPage()),
            ));
          }
        },
        child: Container(
          height: 170 * ratio,
          width: 170 * ratio,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: new DecorationImage(
              image: new FileImage(
                new File('${model.appDir}/categories/q${category.id}.jpg'),
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                // gradient: UIColors().cardGradient,
                color: Color(0xff161616).withOpacity(0.35)),
            child: Padding(
              padding: EdgeInsets.only(right: 15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  UIText(
                    context: context,
                    text: category.name[model.selectedLang],
                  ).foodName,
                  Container(
                    margin: EdgeInsets.only(top: 14),
                    height: 3,
                    width: 20,
                    color: UIColors().primary,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
