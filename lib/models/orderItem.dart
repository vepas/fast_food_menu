import 'package:flutter/material.dart';

class OrderItem {
  final Map<String, dynamic> name;
  final Map<String, dynamic> size;
  final int quantity;
  final int cost;
  OrderItem({
    @required this.name,
    @required this.size,
    @required this.quantity,
    @required this.cost,
  });
}
