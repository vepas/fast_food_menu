import 'package:flutter/material.dart';

import 'orderItem.dart';

class Order {
  final String code;
  final String createdAt;
  final int cost;
  final int serviceCost;
  final int totalCost;
  final List<OrderItem> items;
  bool isExpanded;
  Order(
      {@required this.code,
      @required this.createdAt,
      @required this.cost,
      @required this.serviceCost,
      @required this.totalCost,
      @required this.items,
      this.isExpanded = false});
}
