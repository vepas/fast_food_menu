import 'package:flutter/material.dart';

class Desk {
  final int id;
  final int number;
  final String status;

  Desk({@required this.id, @required this.number, this.status});

  Desk.fromJson(table)
      : id = table['id'],
        number = table['number'],
        status = table['status'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'number': number, 'tables': status};
}
