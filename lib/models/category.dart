import 'package:flutter/material.dart';
import 'food.dart';

class Category {
  final int id;
  final int parentId;
  final String image;
  final Map<String, dynamic> name;
  final bool isDrink;
  List<Food> foods;
  List<Category> subcategories;

  Category(
      {@required this.id,
      this.parentId = 0,
      @required this.image,
      @required this.name,
      @required this.isDrink,
      @required this.subcategories,
      @required this.foods});
}
