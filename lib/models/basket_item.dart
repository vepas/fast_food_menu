import 'food.dart';

class BasketItem {
  final Food food;
  int quantity;

  BasketItem({this.food, this.quantity = 1});
}
