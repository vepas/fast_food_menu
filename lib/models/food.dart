import 'package:flutter/material.dart';

class Food {
  final int id;
  final Map<String, dynamic> name;
  final Map<String, dynamic> size;
  final String image;
  final int imageID;
  final int price;
  final int discountPrice;

  Food({
    @required this.id,
    @required this.name,
    @required this.size,
    @required this.image,
    @required this.imageID,
    @required this.price,
    @required this.discountPrice,
  });

  Food.fromJson({Map<String, dynamic> food, Map<String, dynamic> size})
      : id = size == null ? food['id'] : size['id'],
        name = food['name'],
        size = size == null ? food['size'] : size['name'],
        image = food['image'],
        imageID = size == null ? food['imageID'] : food['id'],
        price = size == null ? food['price'] : size['price'],
        discountPrice =
            size == null ? food['discountPrice'] : size['discount_price'];

  String foodName(String lang, {int summarize}) {
    String newName;
    if (size != null) {
      newName = name[lang] + ', ' + size[lang];
    } else
      newName = name[lang];
    return summarize == null || summarize >= newName.length
        ? newName
        : newName.substring(0, summarize) + '..';
  }

  int get getPrice => discountPrice == null ? price : discountPrice;
}
