import 'package:flutter/material.dart';

import 'desk.dart';

class Hall {
  final int id;
  final Map<String, dynamic> name;
  // final String name;
  final List<Desk> tables;

  Hall({@required this.id, @required this.name, @required this.tables});

  Hall.fromJson(Map<String, dynamic> hall, List<Desk> desks)
      : id = hall['id'],
        name = hall['name'],
        tables = desks;
}
