import '/screens/home.dart';
import '/screens/menu/menu_navigator.dart';
import '/screens/settings/image_downloader.dart';
import '/screens/settings/ip_changer.dart';
import '/screens/settings/password.dart';
import '/screens/settings/settings.dart';
import '/screens/settings/tables.dart';
import '/services/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';

//4096 x 2734

void main() {
  // SystemChrome.setEnabledSystemUIOverlays([]);

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MenuProvider()),
      ],
      child: Phoenix(
        child: MyApp(),
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
  
class _MyAppState extends State<MyApp> {
  double ratio;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.white,
        );
    return Consumer<MenuProvider>(
      builder: (context, model, child) {
        SystemChrome.setEnabledSystemUIOverlays([]);
        return MaterialApp(
          title: 'Fast Food Menu',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            scaffoldBackgroundColor: Color(0xFF1A1D22),
            primaryColor: Color(0xFFFC722D),
            primarySwatch: Colors.orange,
            colorScheme: ColorScheme.dark(),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily: 'DinPro',
          ),
          navigatorKey: model.mainNavKey,
          onGenerateRoute: (settings) {
            Widget page;
            switch (settings.name) {
              case 'password':
                page = PasswordPage();
                break;
              case 'passwordFromTable':
                page = PasswordPage(fromTable: true);
                break;
              case 'settings':
                page = SettingsPage();
                break;
              case 'ip_changer':
                page = IpChangerPage();
                break;
              case 'image_downloader':
                page = ImageDownloaderPage();
                break;
              case 'tables':
                page = TablesPage();
                break;
              case 'menu':
                page = MenuPage();
                break;
              default:
            }
            return PageRouteBuilder(
              pageBuilder: (context, animation, animation1) =>
                  FadeTransition(opacity: animation, child: page),
              transitionDuration: Duration(milliseconds: 100),
              reverseTransitionDuration: Duration(milliseconds: 100),
            );
          },
          home: HomePage(),
        );
      },
    );
  }
}
