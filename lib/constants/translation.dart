Map<String, dynamic> translations = {
  'Category': {
    'tm': 'Kategoriýa',
    'en': 'Category',
    'ru': 'Категория',
  },
  'Enter password': {
    'tm': 'Paroly giriziň',
    'en': 'Enter password',
    'ru': 'Введите пароль',
  },
  'Enter password to get in to settings': {
    'tm': 'Sazlamalara girmek üçin paroly giriziň',
    'en': 'Enter password to get in to settings',
    'ru': 'Введите пароль чтобы перейти к настройкам',
  },
  'Confirm': {
    'tm': 'Tassykla',
    'en': 'Confirm',
    'ru': 'Подтвердить',
  },
  'Settings': {
    'tm': 'Sazlamalar',
    'en': 'Settings',
    'ru': 'Настройки',
  },
  'Change IP': {
    'tm': 'Ip Çalyşmak',
    'en': 'Change IP',
    'ru': 'Изменить IP',
  },
  'Download': {
    'tm': 'Ýüklemek',
    'en': 'Download',
    'ru': 'Скачать',
  },
  'DownloadB': {
    'tm': 'Ýükle',
    'en': 'Download',
    'ru': 'Скачать',
  },
  'Change table': {
    'tm': 'Stoly üýtgetmek',
    'en': 'Change table',
    'ru': 'Сменить стол',
  },
  'Image': {
    'tm': 'Surat',
    'en': 'Image',
    'ru': 'Изображение',
  },
  'Make order': {
    'tm': 'Zakaz etmek',
    'en': 'Make order',
    'ru': 'Заказать',
  },
  'Download images': {
    'tm': 'Suratlary Ýüklemek',
    'en': 'Download images',
    'ru': 'Скачать изображения',
  },
  'Making last changes, please wait a minute': {
    'tm':
        'Menu soňky üýtgeşmeler girizilýär. Biraz\ngaraşmagyňyzy haýyş edýäris',
    'en': 'Making last changes, please\nwait a minute',
    'ru': 'Применяются последние изменения\nпожалуйста подождите немного',
  },
  'Wait...': {
    'tm': 'Garaşyň...',
    'en': 'Wait...',
    'ru': 'Ждите...',
  },
  'Made last changes. You can continue': {
    'tm': 'Menu soňky üýtgeşmeler girzildi. Dowam edip\nbilersiňiz.',
    'en': 'Made last changes. You can\ncontinue',
    'ru': 'Применили последние изменения, можете\nпродолжить',
  },
  'Continue': {
    'tm': 'Dowam et',
    'en': 'Continue',
    'ru': 'Продолжить',
  },
  'Back': {
    'tm': 'Yza gaýt',
    'en': 'Back',
    'ru': 'Назад',
  },
  'Choosen table :': {
    'tm': 'Saýlanan Stol :',
    'en': 'Choosen table :',
    'ru': 'Выбранный стол :',
  },
  'Basket': {
    'tm': 'Sebet',
    'en': 'Basket',
    'ru': 'Корзина',
  },
  'Clear': {
    'tm': 'Arassala',
    'en': 'Clear',
    'ru': 'Очистить',
  },
  'pc.': {
    'tm': 'sany',
    'en': 'pc.',
    'ru': 'шт.',
  },
  'Cost': {
    'tm': 'Umumy',
    'en': 'Cost',
    'ru': 'Всего',
  },
  'Service': {
    'tm': 'Serwis',
    'en': 'Service',
    'ru': 'Сервис',
  },
  'Total Cost': {
    'tm': 'Jemi Bahasy',
    'en': 'Total Cost',
    'ru': 'Итого',
  },
  'All details': {
    'tm': 'Doly Maglumatlary görmek',
    'en': 'All details',
    'ru': 'Подробнее',
  },
  'No choosen meals': {
    'tm': 'Nahar saýlanylmadyk',
    'en': 'No choosen meals',
    'ru': 'Нет выбранных блюд',
  },
  'Empty basket': {
    'tm': 'Sebet boş',
    'en': 'Empty basket',
    'ru': 'Корзина пуста',
  },
  'Add to basket': {
    'tm': 'Sebed goş',
    'en': 'Add to basket',
    'ru': 'В корзину',
  },
  'Show order to waiter': {
    'tm': 'Zakazyňyzy ofisanta görkeziň',
    'en': 'Show order to waiter',
    'ru': 'Покажите заказ официанту',
  },
  'Click the download button for latest update': {
    'tm': 'Iň soňky maglumatlary almak üçin ýüklemek \ndüwmesine basyň',
    'en': 'Click the download button for latest update',
    'ru': 'Нажмите кнопку скачать для загрузки \nпоследних обновлений',
  },
  'Download has been completed': {
    'tm': 'Ýüklemek prosesi tamamlandy',
    'en': 'Download has been completed',
    'ru': 'Загрузка успешно закончена',
  },
  'Meal': {
    'tm': 'Nahar',
    'en': 'Meal',
    'ru': 'Блюдо',
  },
  'Quantity': {
    'tm': 'Sany',
    'en': 'Quantity',
    'ru': 'Количество',
  },
  'Quant.': {
    'tm': 'Sany',
    'en': 'Quant.',
    'ru': 'Колич.',
  },
  'Price': {
    'tm': 'Bahasy',
    'en': 'Price',
    'ru': 'Цена',
  },
  'Selected': {
    'tm': 'Saýlandy',
    'en': 'Selected',
    'ru': 'Добавлен',
  },
  'History': {
    'tm': 'Taryh',
    'en': 'History',
    'ru': 'История',
  },
  'Total Cost (all together) :': {
    'tm': 'Umumy Baha (Bilelikde) :',
    'en': 'Total Cost (all together) :',
    'ru': 'Сумма (все вместе) :',
  },
  'Sent': {
    'tm': 'Ugradyldy',
    'en': 'Sent',
    'ru': 'Отправлен',
  },
  'Order is sent to kitchen and will be ready soon': {
    'tm': 'Zakazyňyz kuhnýa ugradyldy we naharyňyz\ntiz eltip beriler',
    'en': 'Order is sent to kitchen and will be\nready soon',
    'ru': 'Заказ отправлен в кухню\nскоро будет готов',
  },
  'Table': {
    'tm': 'Stol',
    'en': 'Table',
    'ru': 'Стол',
  },
};
