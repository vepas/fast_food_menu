import 'package:flutter/material.dart';

class UIColors {
  final Color _primary = Color(0xfffed931); /* 0xFFFC722D */
  final Color _background = Color(0xFF1A1D22);
  final Color _passive = Color(0xFFABBBC2);
  final Color _cartBackground = Color(0xFF1F2328);
  final Color _basketBackground = Color(0xff1E1E20);

  final Color _cartColor = Color(0xff29292B );
  final Color _stroke = Color(0xFF393C49);
  final Color _foodCardBackground = Color(0xFF2C3137);
  final Color _selectedFood = Color(0xffFED931); /* 0xFF008A63*/
  final LinearGradient _mainGradient = LinearGradient(
    colors: [
      Color(0xFFF89B32),
      Color(0xFFFC542D),
    ],
  );
  final LinearGradient _cardGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      Color(0xFF000000).withOpacity(0),
      Color(0xFF000000),
    ],
  );
  Color get basketBackground => _basketBackground;
  Color get cartColor=> _cartColor;
  Color get primary => _primary;
  Color get background => _background;
  Color get passive => _passive;
  Color get cartBackground => _cartBackground;
  Color get stroke => _stroke;
  Color get foodCardBackground => _foodCardBackground;
  Color get selectedFood => _selectedFood;
  LinearGradient get mainGradient => _mainGradient;
  LinearGradient get cardGradient => _cardGradient;
}
