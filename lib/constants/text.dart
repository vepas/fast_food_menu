import '/constants/colors.dart';
import 'package:flutter/material.dart';

class UIText extends StatelessWidget {
  final String text;
  final Color color;
  final TextAlign align;
  final BuildContext context;
  UIText({
    @required this.context,
    @required this.text,
    this.color,
    this.align = TextAlign.left,
  });

  @override
  Widget build(BuildContext context) {
    return Text(text);
  }

  Text get big {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 42 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get h1 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 28 * ratio,
        height: 1,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get h2 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 20 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get foodName {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 18 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get foodNameDemo {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get bodyText1 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get bodyText2 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      textAlign: align,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 14 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get bodyText3 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 18 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get bodyTextPrice {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 18 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get bodyText4 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14 * ratio,
        color: color == null ? UIColors().primary : color,
      ),
    );
  }

  Text get bodyTextPassive1 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16 * ratio,
        color: color == null ? UIColors().passive : color,
      ),
    );
  }

  Text get bodyTextPassive2 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 16 * ratio,
        color: color == null ? UIColors().passive : color,
      ),
    );
  }

  Text get bodyTextPassive3 {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      textAlign: align,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 14 * ratio,
        color: color == null ? UIColors().passive : color,
      ),
    );
  }

  Text get discountPrice {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 14 * ratio,
        color: color == null ? UIColors().passive : color,
        decoration: TextDecoration.lineThrough,
      ),
    );
  }

  Text get smallText {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14 * ratio,
        color: color == null ? Colors.white : color,
      ),
    );
  }

  Text get smallTextPassive {
    double ratio = MediaQuery.of(context).size.width / 1112;
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 12 * ratio,
        color: color == null ? UIColors().passive : color,
      ),
    );
  }
}
